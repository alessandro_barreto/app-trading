# App Trading Notification

### Formas de receber Notificação.

* Por **Token** do Device [link](https://firebase.google.com/docs/cloud-messaging/android/topic-messaging?authuser=0)
* Por **Tópico** [link](https://firebase.google.com/docs/cloud-messaging/android/client?authuser=0)

### Enviar Notificação (Token) HTTP POST

```
https://fcm.googleapis.com/fcm/send
Content-Type:application/json
Authorization:key=AIzaSyB5IAH9M1PBVrQgbHKH5GgiwFnRJA2KQPU

{
	"data": {
		"title": "Titulo da Notificacao",
		"body": "corpo da notificacao",
		"chave": "valor"
	},
	"to": "token_do_device"
}
```

### Enviar Notificação (Tópico) HTTP POST

```
https://fcm.googleapis.com/fcm/send
Content-Type:application/json
Authorization:key=AIzaSyB5IAH9M1PBVrQgbHKH5GgiwFnRJA2KQPU

{
	"data": {
		"title": "Titulo da Notificacao",
		"body": "corpo da notificacao",
		"chave": "valor"
	},
	"to": "/topics/nome-topico"
}
```


### Observações 

- O Aplicativo está programado para receber (ler) o json com os campos **title** e **body** para montar visualmente a notificação ao usuário.

```java
	@Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData() != null) {
            sendNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
        }
    }
```

- O Aplicativo está programado/se inscrevendo no tópico **trading-alert**

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("trading-alert");

    }
```

- Para enviar notificações por Token é preciso que o servidor esteja preparado para receber o token do dispositivo.